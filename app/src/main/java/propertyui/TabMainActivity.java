package propertyui;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;
import com.wolfsoft.propertyui.R;

import fragment.NearByFragment;


public class TabMainActivity extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    //BottomBar bottomBar;
    FrameLayout frameLayout;
    FrameLayout frameLayout_opiniones;
    private String token,versionName;
    private int versionCode;
    private boolean isCall = false;
    private String hasCard,hasActivated;
    private Dialog slideDialog;
    private TextView tvupdate;
    private TextView tvskip,tvupdatediscription;
    private boolean callUpdateservice;

    private int selectedTabId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        frameLayout = (FrameLayout) findViewById(R.id.framelayout);


        // bottomBar = (BottomBar) findViewById(R.id.bottombar);
       // for (int i = 0; i < bottomBar.getTabCount(); i++) {
       //     bottomBar.getTabAtPosition(i).setGravity(Gravity.CENTER_VERTICAL);
       // }
        frameLayout = (FrameLayout) findViewById(R.id.framelayout);
        frameLayout_opiniones = (FrameLayout) findViewById(R.id.frame_opiniones);

        isCall = true;



        /*roughike bottombar library code is here*/

        /*
        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {

            @Override
            public void onTabSelected(@IdRes int tabId) {
             //   Fragment fragment = null;

//                if (getIntent() != null && getIntent().hasExtra("check")) {
//                    tabId = getIntent().getIntExtra("check", -1);
//                }
                switch (tabId) {
                    case R.id.nearby:
                        selectedTabId =R.id.home;
                        replace_fragment(new NearByFragment());
                        replace_fragment_opiniones(new NearByFragment());

                        break;
                    case R.id.discover:
                        selectedTabId =R.id.discover;
                        replace_fragment(new NearByFragment());

                        break;
                    case R.id.schedule:
                        selectedTabId =R.id.schedule;
                        replace_fragment(new NearByFragment());

                        break;

                    case R.id.favorite:
                        selectedTabId =R.id.favorite;
                       replace_fragment(new NearByFragment());
                        break;
                    case R.id.more:
                        selectedTabId =R.id.more;
                        replace_fragment(new NearByFragment());

                        break;

                }


            }
        });
        */

        replace_fragment(new NearByFragment());
        replace_fragment_opiniones(new NearByFragment());

        // perfil de ususario

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // boton de usuario perfil
        Button boton_usuario= findViewById(R.id.Boton_perfil);
        boton_usuario.setOnClickListener(this);




        // opcion de activar el desplazamiento para sacar la pantalla

        DrawerLayout drawer =  findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView =findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


    }

    @Override
    public void onClick(View view) {
        if (view.getId()==R.id.Boton_perfil){
            DrawerLayout drawer1 = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer1.openDrawer(GravityCompat.START);
        }
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.perfil_usuario, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    // meter las opiniones en otro framelayout---pero sigue en horizontal.Abriaquecambiarlo
    private void replace_fragment_opiniones(NearByFragment nearByFragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_opiniones, nearByFragment);
        transaction.commit();

    }


    public void replace_fragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.framelayout, fragment);
        transaction.commit();
    }




    //////////////////////////////////////////////////////////




}
